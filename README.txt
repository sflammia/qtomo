Update Sep 2014:
Because several people have asked me about this, I’ve decided to put this code in a more visible place 
on BitBucket, and copyrighting it under the MIT license to keep it open source. It has always been 
bundled with arXiv:1205.2300, but now people can fork it and improve on it. You will need to install 
cvx (http://cvxr.com) to get the estimators to work. Enjoy! 



Original readme text from the arXiv version: 

The file FrontEnd.m contains all of the various parameters that the user can set. That file should be 
pretty self explanatory. The code assumes that you have the parallelization toolbox, and possibly also 
the statistics toolbox (I don't recall). There are two global variables in testEstimatorParallel.m 
that need to be set by the user: the directory where the data files will be stored (line 30), and the 
number of cores in which to run in parallel (line 33). Also, the function MakeGraphs.m assumes that 
the data can be found in the current directory in a folder called Data. Sorry for any inconvenience 
this may cause, but file i/o is annoying and time consuming, and it's quite possible that nobody will 
ever read this. :) The final figures for the paper were generated from the Mathematica notebook 
included in the Data folder.

Finally, I want to acknowledge Brielin Brown as a coauthor for some of the functions here, especially 
for the bulk of testEstimatorParallel.m and earlier versions of FrontEnd.m and MakeGraphs.m.